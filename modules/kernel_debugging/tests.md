# Тесты по теме «Отладка на уровне ядра»

Copyright&nbsp;©&nbsp;2023–2024 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Single choice

Какой формат имеет файл модуль ядра?

---

*	exe
*	**elf**
*	ko
*	so

## Single choice

Куда осущуствляет запить функция pr_info в модуле ядра?

---

*	В файл /var/log/kernel.log
*	В файл /var/log/system.log
*	В буфер dmesg
*	**В буфер в памяти**

## Single choice

Какая команда служит для загрузки модуля ядра?

---

*	ld
*	ldd
*	init_mod
*	**insmod**

## Single choice

Можно ли использовать функция printf для выдачи отладочных сообщений в модуле ядра?

---

*	да
*	**нет**
*	можно, если модуль загружен при помощи modprobe
*	можно, если ядро собрано с опцией CONFIG_DYNAMIC_DEBUG

## Single choice

Какая функция модуля ядра вызовется при вызове команды insmod?

---

*	**init_module**
*	main
*	start
*	__init__

## Single choice

Какая функция модуля ядра вызовется при вызове команды rmmod?

---

*	**cleanup_module**
*	main
*	probe
*	release

## Single choice

Что должна возвращать функция init_module в случае успешной инициализации модуля?

---

*	**0**
*	1
*	0xffffffff
*	-1
