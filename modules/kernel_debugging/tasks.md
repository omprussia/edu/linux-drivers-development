# Задания по теме «Отладка на уровне ядра»

Copyright&nbsp;©&nbsp;2023–2024 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Задание

1.	Скомпилируйте ядро c поддержкой отладки
	*	Kernel hacking->Kernel debugging;
	*	Kernel hacking->KGDB:kernel debugger;
	*	Kernel hacking->Compile time checks and compiler options->Provide GDB scripts for kernel debugging.
3.	Создайте initrd/initramfs.
4.	Создайте rootfs (busybox, u-root).
5.	Загрузите ядро в qemu.
6.	Подключитесь к qemu c запущенным Linux при помощи отладчика gdb и выполните пошаговую отладку старта ядра Linux.

Дополнительная информация может быть найдена в [статье](https://www.sobyte.net/post/2022-02/debug-linux-kernel-with-qemu-and-gdb/).
