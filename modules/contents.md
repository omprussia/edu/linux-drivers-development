# Модули для курсов разработки драйверов Linux

Copyright&nbsp;©&nbsp;2023 АНО&nbsp;ВО&nbsp;«Университет Иннополис».
Copyright&nbsp;©&nbsp;2023–2024 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../LICENSE.CC-BY-4.0.ru.md).

*	[Архитектура процессора, введение в&nbsp;X86-64 и&nbsp;ARM&nbsp;assembler. Прерывания. Системные вызовы](./x86_64_assembler_interrupts)
	*	Лекция:
		[cлайды](./x86_64_assembler_interrupts/lecture.fodp)
	*	[Задания](./x86_64_assembler_interrupts/tasks.md)
	*	[Тесты](./x86_64_assembler_interrupts/tests.md)
*	[Задачи в&nbsp;пользовательском пространстве](./userspace)
	*	Лекция:
		[cлайды](./userspace/lecture.fodp)
	*	[Задания](./userspace/tasks.md)
	*	[Лабораторная работа](./userspace/lab.fodp)
	*	[Тесты](./userspace/tests.md)
*	[Отладка пользовательских приложений (gdb)](./debugging_gdb)
	*	Лекция:
		[cлайды](./debugging_gdb/lecture.fodp)
	*	[Задания](./debugging_gdb/tasks.md)
	*	[Лабораторная работа](./debugging_gdb/lab.fodp)
	*	[Тесты](./debugging_gdb/tests.md)
*	[Ядро Linux](./linux_kernel_introduction)
	*	Лекция:
		[cлайды](./linux_kernel_introduction/lecture.fodp)
	*	[Задания](./linux_kernel_introduction/tasks.md)
	*	[Тесты](./linux_kernel_introduction/tests.md)
*	[Загрузка ядра Linux](./booting_the_kernel)
	*	Лекция:
		[cлайды](./booting_the_kernel/lecture.fodp)
	*	[Задания](./booting_the_kernel/tasks.md)
	*	[Лабораторная работа](./booting_the_kernel/lab.fodp)
	*	[Тесты](./booting_the_kernel/tests.md)
*	[Отладка на&nbsp;уровне ядра](./kernel_debugging)
	*	Лекция:
		[cлайды](./kernel_debugging/lecture.fodp)
	*	[Задания](./kernel_debugging/tasks.md)
	*	[Тесты](./kernel_debugging/tests.md)
*	[Модули ядра](./kernel_modules)
	*	Лекция:
		[cлайды](./kernel_modules/lecture.fodp)
	*	[Задания](./kernel_modules/tasks.md)
	*	[Лабораторная работа](./kernel_modules/lab.fodp)
	*	[Тесты](./kernel_modules/tests.md)
*	[Подсистема управления памятью](./memory_managment)
	*	Лекция:
		[cлайды](./memory_managment/lecture.fodp)
	*	[Задания](./memory_managment/tasks.md)
	*	[Тесты](./memory_managment/tests.md)
*	[Подсистема управления процессами, потоки, процессы, диспетчер](./thread_processes_scheduling)
	*	Лекция:
		[cлайды](./thread_processes_scheduling/lecture.fodp)
	*	[Задания](./thread_processes_scheduling/tasks.md)
	*	[Тесты](./thread_processes_scheduling/tests.md)
*	[Параллельный доступ к&nbsp;ресурсам](./concurrent_access_to_resources)
	*	Лекция:
		[cлайды](./concurrent_access_to_resources/lecture.fodp)
	*	[Задания](./concurrent_access_to_resources/tasks.md)
	*	[Тесты](./concurrent_access_to_resources/tests.md)
*	[Аппаратное обеспечение. Шины](./hardware_buses)
	*	Лекция:
		[cлайды](./hardware_buses/lecture.fodp)
	*	[Задания](./hardware_buses/tasks.md)
	*	[Тесты](./hardware_buses/tests.md)
*	[Сетевая подсистема](./net)
	*	Лекция:
		[cлайды](./net/lecture.fodp)
	*	[Задания](./net/tasks.md)
	*	[Тесты](./net/tests.md)
*	[USB подсистема](./usb_subsystem)
	*	Лекция:
		[cлайды](./usb_subsystem/lecture.fodp)
	*	[Задания](./usb_subsystem/tasks.md)
	*	[Лабораторная работа](./usb_subsystem/lab.fodp)
	*	[Тесты](./usb_subsystem/tests.md)
*	[Обзор графического стека в&nbsp;Linux](./graphical_stack)
	*	Лекция:
		[cлайды](./graphical_stack/lecture.fodp)
	*	[Задания](./graphical_stack/tasks.md)
	*	[Тесты](./graphical_stack/tests.md)
