# Тесты по теме «Сетевая подсистема»

Copyright&nbsp;©&nbsp;2023–2024 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Single choice

Какая структура в ядре Linux соответствует сетевому драйверу?

---

*	**struct net_device**
*	strict file_operations
*	struct dev
*	struct cdev

## Single choice

В виде чего обычно оформляется сетевой драйвер в Linux?

---

*	**Модуль ядра**
*	Модуль библиотеки glibc
*	Сервис Systemd
*	демон ядра

## Single choice

Какая структура представляет в сетевом стеке Linux сетевой пакет?

---

*	dev
*	**sk_buff**
*	msg
*	kern_msg

## Single choice

Какая функция в сетевом драйвере отвечает за отправку пакета?

---

*	send
*	ndo_start_xmit
*	pool
*	**обработчик прерывания**
*	Никакая

## Single choice

Какая функция вызывается в сетевом драйвере, в момент создания сокета при помощи вызова stocket в пользовательском приложении?

---

*	open
*	init
*	register
*	**Никакая**

## Single choice

Какая структура используется в современных ядрах для хранения указателей на основные функции сетевого драйвера?

---

*	struct net_device
*	struct file_operations
*	struct dev
*	struct cdev
*	**struct net_device_ops**

## Single choice

В какой функции в сетевого драйверы обычно выделяется sk_buff при приеме Ethernet пакета?

---

*	open
*	ndo_start_xmit
*	Receive
*	**Обработчик прерывания**
*	Ни в какой

## Single choice

Какая функция служит для регистрации структуры net_device в ядре Linux для сетевой карты с интерфейсом pci?

---

*	**register_netdev**
*	pci_register
*	platform_register
*	init_module
*	register_chrdev

## Single choice

В контексте чего работает часть сетевого стека Linux, отвечающая за отправку пакетов?

---

*	Обработчика прерывания сетевого драйвера
*	В контексте вызова send или sendto прикладного приложения
*	**NET_TX SOFTIRQ**
*	В контексте тасклета
