# Задания по теме «Сетевая подсистема»

Copyright&nbsp;©&nbsp;2023–2024 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Задание

1.	Разработать минимальный сетевой драйвер заглушку, который будет:
	*	иметь реализацию функции ndo_start_xmit;
	*	функция ndo_start_xmit будет печатать в 16 формате данные, получаемые в пакете sk_buff.
2.	Разработать пользовательскую программу отправки пакетов, использующую raw сокеты.
3.	Вставить драйвер в ядро и протестировать отправку пакетов при помощи разработанной программы. Убедиться при помощи dmesg, что разработанный драйвер получает пакеты в ndo_start_xmit.
