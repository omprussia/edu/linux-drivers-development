# Тесты по теме «Аппаратное обеспечение. Шины»

Copyright&nbsp;©&nbsp;2023–2024 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Single choice

Какая функция pci драйвера вызывается при успешной вставке в ядро linux, если шина нашла соответвующее устройство?

---

*	**probe**
*	init
*	register_chr_dev
*	register_net_dev

## Single choice

Что обычно используется для сопоставления pci устройства и pci драйвера шиной pci?

---

*	major number
*	MAC адрес
*	**vendor_id, device_id**

## Single choice

Что хранят BAR регистры области конфигурирования pci?

---

*	**адрес памяти вввода-выввода или портов ввода-вывода**
*	номер прерывания
*	адрес памяти и номер прерывания
*	vendor id и device id

## Single choice

Какой адрес хранится в области конфигурирования pci в BAR регистрах?

---

*	виртуальный адрев в адресном пространстве ядра
*	виртуальный адрес в пользовательском пространстве
*	**физический адрес**
