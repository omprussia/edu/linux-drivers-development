# Тесты по теме «USB подсистема»

Copyright&nbsp;©&nbsp;2023–2024 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Single choice

Какая функция usb драйвера вызывается при успешной вставке в ядро linux, если шина нашла соответвующее устройство?

---

*	**probe**
*	init
*	register_chr_dev
*	register_net_dev

## Single choice

Какая структура данных представляет запрос по шине usb в ядре?

---

*	**struct urb**
*	struct usb
*	struct sk_buff


## Single choice

Чем характеризуется конечная точка для шины usb?

---

*	**типом и направлением**
*	vendor_id, device_id
*	major number 

## Single choice

Какой тип передачи должны подерживать все типы usb устройств?

---

*	**control_trensfer**
*	balk_transfer
*	isochronus_trensfer
