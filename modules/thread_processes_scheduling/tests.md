# Тесты по теме «Подсистема управления процессами, потоки, процессы, диспетчер»

Copyright&nbsp;©&nbsp;2023–2024 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Single choice

Какой наиболее полный из перечисленных наборов операций можно выполнять из потока ядра? Какой наиболее полный из перечисленных наборов операций можно выполнять из обработчика прерывания?

---

*	**copy_to_user，kprintf, mutex_lock, wait_event, kmalloc**
*	kprintf, spin_lock
*	Kprintf, kmalloc, spin_lock

## Single choice

Какой наиболее полный из перечисленных наборов операций можно выполнять из обработчика прерывания?

---

*	copy_to_user，kprintf, mutex_lock, wait_event, kmalloc(GFP_KERNEL,...)
*	**kprintf, spin_lock**
*	kprintf, kmalloc(GFP_KERNEL,...), spin_lock

## Single choice

Какой наиболее полный из перечисленных наборов операций можно выполнять из тасклета?

---

*	copy_to_user，kprintf, mutex_lock, wait_event, kmalloc(GFP_KERNEL,...)
*	**kprintf, spin_lock**
*	Kprintf, kmalloc(GFP_KERNEL,...), spin_lock

## Single choice

В каком из перечисленных контекстов нельзя выполнять функцию copy_to_user?

---

*	**В обработчике прерывания**
*	В потоке ядра
*	В функции ioctl символьного драйвера

## Single choice

В каком из перечисленных контекстов нельзя отдавать управление диспетчеру?

---

*	**В обработчике прерывания**
*	В потоке ядра
*	В функции ioctl символьного драйвера

## Single choice

В каком из перечисленных вызовов используется циклический опрос переменно при помощи атомарных операций?

---

*	Mutex_lock
*	**Spin_lock**
*	Wait_queue_interaptible

## Single choice

Какой механизм используют символьные драйверы для организации блокирующих операций?

---

*	Mutex_lock
*	Spin_lock
*	**Wait_queue_interaptible**

## Single choice

В какое состояние переходит задача, вызвавшая блокирующую операцию в случае, если в  вызове Wait_queue_interaptible условие не выполняется?

---

*	TASK_RUNNING
*	**TASK_INTERRUPTIBLE**
*	TASK_ZOMBIE
*	TASK_UNINTERRUPTIBLE
