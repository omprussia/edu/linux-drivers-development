# Задания по теме «Задачи в пользовательском пространстве»

Copyright&nbsp;©&nbsp;2023–2024 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Задание

1.	Разработать API для работы с логами dbgwrite, dbginit, dbgwrite dbgclose. API должно позволять записывать отладочные сообщения в циклический буфер в разделяемой памятьи (SHM).
2.	Реализовать API в виде разделяемой библиотеки.
3.	Написать пример использования библиотеки.

Пример работы с разделяемой памятью: 

```cpp
#define SHM_NAME "my_shm"
#define SHM_SIZE sizeof(struct shmstruct)

int child(void);
int parent(pid_t);

struct shmstruct {
  sem_t parent_done;
  sem_t child_done;
  char msg[256];
};
struct shmstruct *ptr;
int main()
{
  int shm_fd;
  pid_t cpid, pid;
  shm_fd = shm_open(SHM_NAME, O_CREAT | O_RDWR, 0666));
  ftruncate(shm_fd, SHM_SIZE);
  ptr = (struct shmstruct *) mmap(0, SHM_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0));
  close(shm_fd);

  imlock(ptr, SHM_SIZE);
  sem_init(&ptr->parent_done, 1, 0);
  sem_init(&ptr->child_done, 1, 0);
  if ((cpid = fork()) == 0)
    child();
  else if(cpid > 0) {
    pid = getpid();
    sprintf(ptr->msg, "message from parent");
    sem_post(&ptr->parent_done);
    sem_wait(&ptr->child_done);
    printf("%ld read: %s\n", (long)pid, ptr->msg);
  }
  wait(cpid);
  sem_destroy(&ptr->parent_done);
  sem_destroy(&ptr->child_done);
  munmap(ptr, SHM_SIZE);
  shm_unlink(SHM_NAME);
  return 0;
}

int child()
{
  pid_t pid = getpid();
  sem_wait(&ptr->parent_done);
  printf("%ld read: %s\n", (long)pid, ptr->msg);
  sprintf(ptr->msg, "message from child");
  sem_post(&ptr->child_done);
}
```
